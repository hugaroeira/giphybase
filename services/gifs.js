import axios from 'axios';


const requestURL = 'https://api.giphy.com/v1/gifs/';
const apiKey = 'vHvCxQ3XNfBjWaD3v7x60NuvzvpcmY86';


export default {


    searchGifs: (limit, searchSentence, callback) => {

        const urlSearch = requestURL + 'search?api_key=' + apiKey + '&q=' + searchSentence + '&limit=' + limit + '&offset=0&rating=g&lang=pt';
        axios.get(urlSearch).then((gifs) => {
            if (callback) {
                callback(gifs);
            }
        })
    },

    getById: (id, callback) => {

        const urlGet = requestURL + id + '?api_key=' + apiKey;
        axios.get(urlGet).then((gifInfos) => {
            if (callback) {
                callback(gifInfos);
            }
        })
    },



}