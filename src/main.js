import Vue from 'vue';
import App from './App.vue';
import VueObserveVisibility from 'vue-observe-visibility';
import VueRouter from 'vue-router';
import { routes } from './routes';


Vue.use(VueObserveVisibility);
Vue.use(VueRouter);

const router = new VueRouter({ routes })

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
